package uk.co.ribot.androidboilerplate.util;

/**
 * Created by ilyae on 12.07.2017.
 */

public final class AuthUtil {

    public static boolean isValidEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static boolean isStrongPassword(String password){
        return password.matches("^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,}");
    }
}