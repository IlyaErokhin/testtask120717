package uk.co.ribot.androidboilerplate.util;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ilyae on 12.07.2017.
 */

public interface WheatherAPI {

    @GET("/premium/v1/weather.ashx")
    Call<ResponseBody> getWheather(@Query("key") String key,
                                   @Query("q") String city,
                                   @Query("format") String format);
}
