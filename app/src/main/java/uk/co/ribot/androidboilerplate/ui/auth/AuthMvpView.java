package uk.co.ribot.androidboilerplate.ui.auth;

import android.view.View;

import uk.co.ribot.androidboilerplate.ui.base.MvpView;

/**
 * Created by ilyae on 11.07.2017.
 */

public interface AuthMvpView extends MvpView {

    void showError(View v, String message);

    void getWheather(View v);

    void showWheather(View v, String message);
}
