package uk.co.ribot.androidboilerplate.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.ribot.androidboilerplate.R;
import uk.co.ribot.androidboilerplate.ui.base.BaseActivity;
import uk.co.ribot.androidboilerplate.util.AuthUtil;
import uk.co.ribot.androidboilerplate.util.WheatherAPI;

/**
 * Created by ilyae on 11.07.2017.
 */

public class AuthActivity extends BaseActivity implements AuthMvpView, View.OnClickListener {

    @Inject AuthPresenter mAuthPresenter;

    @BindView(R.id.enter_btn) Button mEnterButton;
    @BindView(R.id.etEmail) EditText mEmailEditText;
    @BindView(R.id.etPassword) EditText mPasswordEditText;

    private static WheatherAPI wheatherApi;
    private Retrofit retrofit;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, AuthActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mEnterButton.setOnClickListener(this);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://api.worldweatheronline.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        wheatherApi = retrofit.create(WheatherAPI.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_auth, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_create);
        if (item != null)
            item.setEnabled(true);
        return super.onPrepareOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.enter_btn:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                if(!AuthUtil.isValidEmail(mEmailEditText.getText().toString())){
                    showError(v,getResources().getString(R.string.error_email));
                    return;
                }

                if(!AuthUtil.isStrongPassword(mPasswordEditText.getText().toString())){
                    showError(v,getResources().getString(R.string.error_password));
                    return;
                }

                getWheather(v);

                break;
        }
    }

    @Override
    public void showError(View v, String message) {
        Snackbar snackbar = Snackbar.make(v, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.error_color));
        snackbar.show();
    }

    @Override
    public void getWheather(final View v) {
        wheatherApi.getWheather("db35c917300a42d49b1121615171207", "Moscow", "json").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    // Прошу прощение за следующие 4 строки, было жалко времени на реализацию объекта, который возвращается в JSON
                    JSONObject obj = new JSONObject(response.body().string().toString());
                    obj = new JSONObject(obj.getString("data"));
                    obj = new JSONObject(obj.getString("current_condition").substring(1,obj.getString("current_condition").length()-1));
                    showWheather(v, String.format(getResources().getString(R.string.wheather_text), obj.getString("temp_C")));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    @Override
    public void showWheather(View v, String message) {
        Snackbar snackbar = Snackbar.make(v, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.light_accent));
        snackbar.show();
    }
}
